import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.mail.*;
import javax.mail.internet.*;

/**
 * Servlet implementation class Hello
 */
@WebServlet("/send_email")
public class SendEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String toAddress = "zeyad.gasser@gmail.com";

	public SendEmail() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean emailSent = sendEmail(request.getParameter("email"), toAddress, 
				request.getParameter("subject"), request.getParameter("note"));
		response.getWriter().append(String.format("[{success: %s}]", emailSent));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public boolean sendEmail(String fromAddr, String toAddr, String subject, String body){
		Properties props = new Properties();
		//sample connection from Google
		props.put("mail.smtp.host", "Page on gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"Page on javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("YOUR_EMAIL_SERVER_USERNAME",
						"YOUR_EMAIL_SERVER_PASSWORD");
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromAddr));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddr));
			message.setSubject(subject);
			message.setText(body);

			Transport.send(message);
			return true;
		} catch (MessagingException e) {
			System.out.println("unable to send email: " + e);
			return true;
		}
	}

	static void sendEmail(
			String smtpAddress, 
			String smtpPort, 
			String enableTLS, 
			String enableAuth, 
			final String username, 
			final String password,
			String fromAddress,
			String toAddress,
			String mySubject,
			String myMessage){

		Properties props = new Properties();
		props.put("mail.smtp.starttls.enable", enableTLS);
		props.put("mail.smtp.auth", enableAuth);
		props.put("mail.smtp.host", smtpAddress);
		props.put("mail.smtp.port", smtpPort);

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromAddress));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(toAddress));
			message.setSubject(mySubject);
			message.setText(myMessage);
			Transport.send(message);
			System.out.print("Sent");

		} catch (Exception e) {
			System.out.print(e);
		}
	}
}
